package it.cipi.labs.virtualobject.data;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VirtualObject {

	@SerializedName("name")
	@Expose
	private String name;
	
	@SerializedName("url")
	@Expose
	private String url;
	
	@SerializedName("events")
	@Expose
	private ArrayList<String>events;
	
	@SerializedName("actions")
	@Expose
	private ArrayList<String>actions;

	public VirtualObject(String name, String url, ArrayList<String> events, ArrayList<String> actions) {
		super();
		this.name = name;
		this.url = url;
		this.events = events;
		this.actions = actions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ArrayList<String> getEvents() {
		return events;
	}

	public void setEvents(ArrayList<String> events) {
		this.events = events;
	}

	public ArrayList<String> getActions() {
		return actions;
	}

	public void setActions(ArrayList<String> actions) {
		this.actions = actions;
	}
	
	
	
	
}
